import pygame
import sys
import time




gameover = False
size = width, height = 1080, 640
screen = None
bg = None


def handle_events():
    global gameover
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameover = True


def init():
    global screen, bg
    pygame.init()
    bg = (0, 0, 0)
    screen = pygame.display.set_mode(size)

def main():
    ball = pygame.image.load("bs.png")
    ballrect = ball.get_rect()
    ballrect.x = 100
    ballrect.y = 100
    ball = pygame.transform.scale(ball, (50, 50))
    init()
    sost = 1
    vx = 3
    vy = 3
    while not gameover:
        handle_events()
        screen.fill(bg)
        screen.blit(ball, ballrect)
        ballrect.x += vx
        ballrect.y += vy
        if ballrect.x + 50 > width or ballrect.x - 50 < 0:
            vx *= -1
        if ballrect.y + 50 > height or ballrect.y - 50 < 0:
            vy *= -1
        pygame.display.flip()


main()
